Notepad++ v8.7.3 bug-fixes & new features :

1. Fix a crash while disabling "Pin tab" feature.
2. Fix drag&drop a folder in Notepad++ launch redundant dialog regression.
3. Fix docked panels invisibility in multi-instance mode.
4. Add "Pin/Unpin Tab" context menu item.
5. Add "Close All BUT Pinned" command.
6. Fix a possible buffer overflow issue.


Notepad++ v8.7.2 new features & bug-fixes:

1. Add Pin tab feature.
2. Tabbar enhancement: Hide inactive tab Close & Pin buttons.  
3. Tabbar enhancement: Highlight inactive darken tab on mouse hover.
4. Fix Ctrl-C not doing copy from Search result issue.
5. Add "Minimize / Close to" option for System tray.
6. Add ability to open/copy selected files from Search-results.
7. Fix replace field focus losing when Notepad++ is switched back.


Get more info on
https://notepad-plus-plus.org/downloads/v8.7.3/


Included plugins:

1.  NppExport v0.4
2.  Converter v4.6
3.  Mime Tool v3.1


Updater (Installer only):

* WinGUp (for Notepad++) v5.3.1
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C and Misc

0001 xtal ext crystal connect
0001 aref analog reference
0001 tdi tdo tms tck is jtag interface
0001 ss mosi miso sck = spi protocol (master slave in master slave out)
0001 sck sdk pins i2c protocol-
0001 rxd txd uart pins
0001 oc0 oc1 output compare of timers
0001 icp input capture pins
0001 ain0 analog inputs comparator
0001 2 stage pipling relate to latency and throughput
0001 fuse and lock bits of the atmega32 mcu deal with mem protection prog mode start
0001 time clock source selection (this connectiokn with prev note)
0001 fuse and lock bits reduce additional hware -- controlled via software
0001 fuse bit prog 0 is enable 1 means disable
0001
0001
0001
0001
0001
0001

///////////////////////////////////////////////////////////////////////////////
// GITLAB

0001 ssh-keygen -t ed25519 -C "gitlab key pair"
0001 cat the key in the .ssh folder
0001 the ssh -T git@gitlab.com and this will connect
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001

 

///////////////////////////////////////////////////////////////////////////////
// C++

0001 ssh-keygen -t ed25519 -C "gitlab key pair"
0001 cat the key in the .ssh folder
0001 the ssh -T git@gitlab.com and this will connect
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001

///////////////////////////////////////////////////////////////////////////////
// MatLab

0001 ssh-keygen -t ed25519 -C "gitlab key pair"
0001 cat the key in the .ssh folder
0001 the ssh -T git@gitlab.com and this will connect
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001
0001


///////////////////////////////////////////////////////////////////////////////
// Git

001. gitLab is a scode manangment system and a devops platform // scope
002. git init test-project                                     // name the dir for proj
003. git config --global init.defaultBranch main               // config
004. git branch -m main                                        // checkout to main
005. git status                                                // now on my feature branch
006. git stage is for working on files that are related        // stages git has 2 stage commit
007. git commit -m "message"                                   // staging comm change that are logic related
008. git working dir file is red stagin yellow repos blue      // stage changes that are related to each other
009. git rm --cached <file>                                    // to unstage
010. git log                                                   // show history
011. git branch my-feature                                     // feature
012. git checkout my-feature                                   // checkout a branch
013. git restore                                               // return file discard changes
014. git merge my-feature                                      // restore
015. git log --all --oneline                                   // show history
016. git stash push                                            // hides changes from git
017. git stash apply                                           // related
018. git stash pop or clear                                    // related
019. git lab project is essentially a container for git repos  // container
020. git lab project has collaboration tools like merge req    // tools
021. git lab project merge reque provide space for members     // to discuss changes in source
022. git lab project issue is a way to track work              // issues can be used to report bugs, track tasks, request new feature
023. git lab project issue way also to ask questions           // sw dev workflow should start with an issue
024. git git not needed here ssh-keygen -t ed25519 -C"GL ky p" // ssh
025. git to get key pair cat ~/.ssh/id_ed25519.pub             // ssh
026. git to check pair ssh -T git@gitlab.com                   // ssh
027. git lab runner is something that links to ci cd           // ci cd
028. git lab flow is a branching strategy                      // green belt
029. git is a dist system must sync git with gitlab            // distributed system
030. git push -u origin branchname                             // key
031. git tag -a v1.2 -m "version 1.2"                          // tag
032. git push origin v1.2                                      // key
033. git branch -d workBranch                                  // branch
034. git branch --all                                          // shows remote and local branches
035. git pull origin main                                      // this can pull to a new directory
036. git remote add origin https://gitlab.com/jmmworknotes.git // this must be done first for 35
038. git clone with ssh line from gitlab will also work        // to pull from gitlab
039. git tag -- create a prod release with a tag               // to pull from gitlab
040. git branch -d readme-branch                               // deletes branch
041. git practice and refresher work of 9/17                   // to pull from gitlab
042. git practice and refresher work of 9/19                   // to pull from gitlab
043. git troubles and password with storm gitlab               // to pull from gitlab
044. git clone with ssh line from gitlab will also work        // to pull from gitlab
045. git restore                                               // general
045. git config --global color.ui auto                         // git color to ui
045. git change # 1                                            // general
045. git stash                                                 // general
045. git config --global --list                                // list all configs
045. git restore                                               // general
045. git restore                                               // general
045. git restore                                               // general
045. git restore                                               // general
045. git restore                                               // general
045. git restore                                               // general
045. git restore   

